INSERT INTO account (id, balance) VALUES (NEXTVAL('account_seq'), 200.5);
INSERT INTO client (id, firstname, lastname, email, password, account_id) VALUES (NEXTVAL('client_seq'), 'firstname', 'lastname', 'account@kata.com', '$2a$10$v5.p6K27UdID3SOYOR1WMe5rECxfYfwHU4OhOSQF2ReEaVNnt6MzK', CURRVAL('account_seq'));
