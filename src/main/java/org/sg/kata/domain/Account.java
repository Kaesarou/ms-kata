package org.sg.kata.domain;

import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
public interface Account {

    AccountId id();

    Amount balance();

    List<Operation> history();
}
