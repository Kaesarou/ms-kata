package org.sg.kata.domain;

import org.immutables.value.Value;

@Value.Immutable
public interface Amount {
    @Value.Parameter
    Double value();

    default Amount plus(Amount amount) {
        return ImmutableAmount.of(value() + amount.value());
    }

    default Amount minus(Amount amount) {
        return ImmutableAmount.of(value() - amount.value());
    }

    default boolean isNegative() {
        return value() < 0;
    }
}
