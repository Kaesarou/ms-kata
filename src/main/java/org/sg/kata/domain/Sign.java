package org.sg.kata.domain;

public enum Sign {
    PLUS("+"), MINUS("-");

    private final String symbol;

    Sign(String symbol) {
        this.symbol = symbol;
    }

    public String symbol() {
        return this.symbol;
    }
}
