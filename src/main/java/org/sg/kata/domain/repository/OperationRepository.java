package org.sg.kata.domain.repository;

import org.sg.kata.domain.OperationId;

public interface OperationRepository {

    OperationId findNextId();
}
