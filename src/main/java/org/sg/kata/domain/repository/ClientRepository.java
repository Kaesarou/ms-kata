package org.sg.kata.domain.repository;

import org.sg.kata.domain.Client;
import org.sg.kata.domain.ClientId;

import java.util.Optional;

public interface ClientRepository {

    ClientId findNextId();

    Optional<Client> findById(ClientId id);

    Optional<Client> findByUsername(String username);

    Client save(Client client);
}
