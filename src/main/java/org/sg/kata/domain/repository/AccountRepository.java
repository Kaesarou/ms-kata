package org.sg.kata.domain.repository;

import org.sg.kata.domain.Account;
import org.sg.kata.domain.AccountId;

import java.util.Optional;

public interface AccountRepository {

    AccountId findNextId();

    Optional<Account> findById(AccountId id);

    Account save(Account account);
}
