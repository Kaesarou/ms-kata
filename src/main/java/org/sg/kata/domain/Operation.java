package org.sg.kata.domain;

import org.immutables.value.Value;

import java.time.LocalDateTime;

@Value.Immutable
public interface Operation {

    OperationId id();

    LocalDateTime date();

    Sign sign();

    Amount amount();
}
