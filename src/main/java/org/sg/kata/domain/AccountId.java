package org.sg.kata.domain;

import org.immutables.value.Value;

@Value.Immutable
public interface AccountId {
    @Value.Parameter
    Long value();
}
