package org.sg.kata.domain.command.handler;

import org.sg.kata.domain.command.Command;

public interface CommandHandler<T extends Command, R> {

    R handle(T t);
}
