package org.sg.kata.domain.command.handler;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import org.sg.kata.domain.*;
import org.sg.kata.domain.command.Withdrawal;
import org.sg.kata.domain.exception.NotAllowedOperation;
import org.sg.kata.domain.exception.UnexistingAccount;
import org.sg.kata.domain.repository.AccountRepository;
import org.sg.kata.domain.repository.OperationRepository;

import java.time.LocalDateTime;

import static org.sg.kata.domain.Sign.MINUS;

public class WithdrawalHandler implements CommandHandler<Withdrawal, Account> {

    private final AccountRepository accountRepository;
    private final OperationRepository operationRepository;

    public WithdrawalHandler(AccountRepository accountRepository, OperationRepository operationRepository) {
        this.accountRepository = accountRepository;
        this.operationRepository = operationRepository;
    }

    @Override
    public Account handle(Withdrawal withdrawal) {
        Account account = accountRepository.findById(withdrawal.accountId())
                .orElseThrow(UnexistingAccount::new);

        Amount amount = account.balance().minus(withdrawal.amount());
        if (amount.isNegative()) {
            throw new NotAllowedOperation();
        }

        Operation operation = ImmutableOperation.builder()
                .id(this.operationRepository.findNextId())
                .date(LocalDateTime.now())
                .amount(withdrawal.amount())
                .sign(MINUS)
                .build();

        Account newAccount = ImmutableAccount
                .copyOf(account)
                .withBalance(amount)
                .withHistory(Stream.concat(List.of(operation), account.history()));
        this.accountRepository.save(newAccount);
        return newAccount;
    }
}
