package org.sg.kata.domain.command;

import org.immutables.value.Value;
import org.sg.kata.domain.Amount;
import org.sg.kata.domain.AccountId;

@Value.Immutable
public interface Deposit extends Command {

    AccountId accountId();

    Amount amount();
}
