package org.sg.kata.domain.command.handler;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import org.sg.kata.domain.Account;
import org.sg.kata.domain.ImmutableAccount;
import org.sg.kata.domain.ImmutableOperation;
import org.sg.kata.domain.Operation;
import org.sg.kata.domain.command.Deposit;
import org.sg.kata.domain.exception.UnexistingAccount;
import org.sg.kata.domain.repository.AccountRepository;
import org.sg.kata.domain.repository.OperationRepository;

import java.time.LocalDateTime;

import static org.sg.kata.domain.Sign.PLUS;

public class DepositHandler implements CommandHandler<Deposit, Account> {

    private final AccountRepository accountRepository;
    private final OperationRepository operationRepository;

    public DepositHandler(AccountRepository accountRepository,
                          OperationRepository operationRepository) {
        this.accountRepository = accountRepository;
        this.operationRepository = operationRepository;
    }

    @Override
    public Account handle(Deposit deposit) {
        Account account = accountRepository.findById(deposit.accountId())
                .orElseThrow(UnexistingAccount::new);
        Operation operation = ImmutableOperation.builder()
                .id(this.operationRepository.findNextId())
                .date(LocalDateTime.now())
                .amount(deposit.amount())
                .sign(PLUS)
                .build();
        Account newAccount = ImmutableAccount
                .copyOf(account)
                .withBalance(account.balance().plus(deposit.amount()))
                .withHistory(Stream.concat(List.of(operation), account.history()));
        this.accountRepository.save(newAccount);
        return newAccount;
    }
}
