package org.sg.kata.domain;

import org.immutables.value.Value;

@Value.Immutable
public interface OperationId {

    @Value.Parameter
    Long value();
}
