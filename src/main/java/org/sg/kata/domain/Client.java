package org.sg.kata.domain;

import org.immutables.value.Value;

@Value.Immutable
public interface Client {

    ClientId id();

    String firstname();

    String lastname();

    String email();

    String password();

    Account account();
}
