package org.sg.kata.domain;

import org.immutables.value.Value;

@Value.Immutable
public interface ClientId {
    @Value.Parameter
    Long value();
}
