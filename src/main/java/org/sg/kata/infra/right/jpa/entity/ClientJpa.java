package org.sg.kata.infra.right.jpa.entity;

import lombok.Data;
import org.sg.kata.domain.Client;
import org.sg.kata.domain.ImmutableClient;
import org.sg.kata.domain.ImmutableClientId;

import javax.persistence.*;

@Entity
@Data
@Table(name = "client")
public class ClientJpa {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_generator")
    @SequenceGenerator(name = "client_generator", sequenceName = "client_seq")
    private Long id;
    private String firstname;
    private String lastname;
    @Column(unique = true)
    private String email;
    private String password;
    @OneToOne
    private AccountJpa account;

    public Client map() {
        return ImmutableClient.builder()
                .id(ImmutableClientId.of(this.id))
                .firstname(this.firstname)
                .lastname(this.lastname)
                .email(this.email)
                .password(this.password)
                .account(this.account.map())
                .build();
    }

    public static ClientJpa from(Client client) {
        ClientJpa jpaEntity = new ClientJpa();
        jpaEntity.setId(client.id().value());
        jpaEntity.setFirstname(client.firstname());
        jpaEntity.setLastname(client.lastname());
        jpaEntity.setEmail(client.email());
        jpaEntity.setPassword(client.password());
        jpaEntity.setAccount(AccountJpa.from(client.account()));
        return jpaEntity;
    }
}
