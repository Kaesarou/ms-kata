package org.sg.kata.infra.right.jpa.entity;

import lombok.Data;
import org.sg.kata.domain.Account;
import org.sg.kata.domain.ImmutableAccount;
import org.sg.kata.domain.ImmutableAccountId;
import org.sg.kata.domain.ImmutableAmount;

import javax.persistence.*;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Entity
@Data
@Table(name = "account")
public class AccountJpa {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_generator")
    @SequenceGenerator(name = "account_generator", sequenceName = "account_seq")
    private Long id;
    private Double balance;
    @OneToMany(cascade = CascadeType.ALL)
    private List<OperationJpa> history;

    public Account map() {
        return ImmutableAccount.builder()
                .id(ImmutableAccountId.of(this.id))
                .balance(ImmutableAmount.of(this.balance))
                .history(this.history.stream().map(OperationJpa::map).collect(toList()))
                .build();
    }

    public static AccountJpa from(Account account) {
        AccountJpa jpaEntity = new AccountJpa();
        jpaEntity.setId(account.id().value());
        jpaEntity.setBalance(account.balance().value());
        jpaEntity.setHistory(account.history().stream().map(OperationJpa::from).collect(toList()));
        return jpaEntity;
    }
}
