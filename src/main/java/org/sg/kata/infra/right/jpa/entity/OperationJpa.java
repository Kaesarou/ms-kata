package org.sg.kata.infra.right.jpa.entity;

import lombok.Data;
import org.sg.kata.domain.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "operation")
public class OperationJpa {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operation_generator")
    @SequenceGenerator(name = "operation_generator", sequenceName = "operation_seq")
    private Long id;
    private LocalDateTime date;
    @Enumerated(EnumType.STRING)
    private Sign sign;
    private Double amount;

    public Operation map() {
        return ImmutableOperation.builder()
                .id(ImmutableOperationId.of(this.id))
                .date(this.date)
                .sign(this.sign)
                .amount(ImmutableAmount.of(this.amount))
                .build();
    }

    public static OperationJpa from(Operation operation) {
        OperationJpa jpaEntity = new OperationJpa();
        jpaEntity.setId(operation.id().value());
        jpaEntity.setDate(operation.date());
        jpaEntity.setSign(operation.sign());
        jpaEntity.setAmount(operation.amount().value());
        return jpaEntity;
    }
}
