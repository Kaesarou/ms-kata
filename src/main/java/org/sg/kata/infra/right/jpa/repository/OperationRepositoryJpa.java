package org.sg.kata.infra.right.jpa.repository;

import org.sg.kata.domain.ImmutableOperationId;
import org.sg.kata.domain.OperationId;
import org.sg.kata.domain.repository.OperationRepository;
import org.sg.kata.infra.right.jpa.entity.OperationJpa;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationRepositoryJpa extends OperationRepository, CrudRepository<OperationJpa, Long> {

    @Query(value = "SELECT operation_seq.nextval FROM dual", nativeQuery = true)
    Long getNextSeriesId();

    @Override
    default OperationId findNextId() {
        return ImmutableOperationId.of(this.getNextSeriesId());
    }
}
