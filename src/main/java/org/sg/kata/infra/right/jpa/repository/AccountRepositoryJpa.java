package org.sg.kata.infra.right.jpa.repository;

import org.sg.kata.domain.Account;
import org.sg.kata.domain.AccountId;
import org.sg.kata.domain.ImmutableAccountId;
import org.sg.kata.domain.repository.AccountRepository;
import org.sg.kata.infra.right.jpa.entity.AccountJpa;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepositoryJpa extends AccountRepository, CrudRepository<AccountJpa, Long> {

    @Query(value = "SELECT account_seq.nextval FROM dual", nativeQuery = true)
    Long getNextSeriesId();

    @Override
    @Query("SELECT a FROM AccountJpa a LEFT JOIN FETCH a.history h WHERE a.id = :id")
    Optional<AccountJpa> findById(@Param("id") Long id);

    @Override
    default AccountId findNextId() {
        return ImmutableAccountId.of(this.getNextSeriesId());
    }

    @Override
    default Optional<Account> findById(AccountId id) {
        return this.findById(id.value()).map(AccountJpa::map);
    }

    @Override
    default Account save(Account account) {
        return this.save(AccountJpa.from(account)).map();
    }
}
