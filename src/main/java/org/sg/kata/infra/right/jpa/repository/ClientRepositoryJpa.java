package org.sg.kata.infra.right.jpa.repository;

import org.sg.kata.domain.Client;
import org.sg.kata.domain.ClientId;
import org.sg.kata.domain.ImmutableClientId;
import org.sg.kata.domain.repository.ClientRepository;
import org.sg.kata.infra.right.jpa.entity.ClientJpa;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepositoryJpa extends ClientRepository, CrudRepository<ClientJpa, Long> {

    @Query(value = "SELECT client_seq.nextval FROM dual", nativeQuery = true)
    Long getNextSeriesId();

    Optional<ClientJpa> findByEmail(String email);

    @Override
    default ClientId findNextId() {
        return ImmutableClientId.of(this.getNextSeriesId());
    }

    @Override
    default Optional<Client> findById(ClientId id) {
        return this.findById(id.value()).map(ClientJpa::map);
    }

    @Override
    default Optional<Client> findByUsername(String username) {
        return this.findByEmail(username).map(ClientJpa::map);
    }

    @Override
    default Client save(Client client) {
        return this.save(ClientJpa.from(client)).map();
    }
}
