package org.sg.kata.infra.left.dto;

import lombok.Data;

@Data
public class AuthenticationDto {

    private String username;
    private String password;
}
