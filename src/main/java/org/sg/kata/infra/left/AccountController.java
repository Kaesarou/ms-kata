package org.sg.kata.infra.left;

import org.sg.kata.infra.configuration.security.ClientToken;
import org.sg.kata.infra.left.dto.AccountDto;
import org.sg.kata.infra.left.dto.AmountDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("account")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping()
    public ResponseEntity<AccountDto> history(ClientToken token) {
        return ResponseEntity.ok(this.accountService.showAccount(token.getAccountId()));
    }

    @PostMapping("deposit")
    public ResponseEntity<Void> deposit(ClientToken token, @RequestBody AmountDto amountDto) {
        this.accountService.deposit(token.getAccountId(), amountDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("withdrawal")
    public ResponseEntity<Void> withdrawal(ClientToken token, @RequestBody AmountDto amountDto) {
        this.accountService.withdrawal(token.getAccountId(), amountDto);
        return ResponseEntity.ok().build();
    }
}
