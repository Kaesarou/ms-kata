package org.sg.kata.infra.left.dto;

import lombok.Data;
import org.sg.kata.domain.Client;

@Data
public class ClientDto {

    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private AccountDto account;

    public static ClientDto from(Client client) {
        ClientDto dto = new ClientDto();
        dto.setId(client.id().value());
        dto.setFirstname(client.firstname());
        dto.setLastname(client.lastname());
        dto.setEmail(client.email());
        dto.setAccount(AccountDto.from(client.account()));
        return dto;
    }
}
