package org.sg.kata.infra.left.dto;

import lombok.Data;
import org.sg.kata.domain.Account;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
public class AccountDto {

    private Long id;
    private Double balance;
    private List<OperationDto> history;

    public static AccountDto from(Account account) {
        AccountDto dto = new AccountDto();
        dto.setId(account.id().value());
        dto.setBalance(account.balance().value());
        dto.setHistory(account.history().stream().map(OperationDto::from).collect(toList()));
        return dto;
    }
}
