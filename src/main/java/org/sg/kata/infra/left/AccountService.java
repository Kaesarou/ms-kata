package org.sg.kata.infra.left;

import org.sg.kata.domain.Account;
import org.sg.kata.domain.AccountId;
import org.sg.kata.domain.ImmutableAccountId;
import org.sg.kata.domain.ImmutableAmount;
import org.sg.kata.domain.command.Deposit;
import org.sg.kata.domain.command.ImmutableDeposit;
import org.sg.kata.domain.command.ImmutableWithdrawal;
import org.sg.kata.domain.command.Withdrawal;
import org.sg.kata.domain.command.handler.CommandHandler;
import org.sg.kata.domain.exception.UnexistingAccount;
import org.sg.kata.domain.repository.AccountRepository;
import org.sg.kata.infra.left.dto.AccountDto;
import org.sg.kata.infra.left.dto.AmountDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AccountService {

    private final CommandHandler<Deposit, Account> depositCommandHandler;
    private final CommandHandler<Withdrawal, Account> withdrawalCommandHandler;
    private final AccountRepository accountRepository;


    public AccountService(CommandHandler<Deposit, Account> depositCommandHandler, CommandHandler<Withdrawal, Account> withdrawalCommandHandler, AccountRepository accountRepository) {
        this.depositCommandHandler = depositCommandHandler;
        this.withdrawalCommandHandler = withdrawalCommandHandler;
        this.accountRepository = accountRepository;
    }

    public void deposit(Long accountId, AmountDto amountDto) {
        Deposit deposit = ImmutableDeposit.builder()
                .accountId(ImmutableAccountId.of(accountId))
                .amount(ImmutableAmount.of(amountDto.getAmount()))
                .build();
        depositCommandHandler.handle(deposit);
    }

    public void withdrawal(Long accountId, AmountDto amountDto) {
        Withdrawal withdrawal = ImmutableWithdrawal.builder()
                .accountId(ImmutableAccountId.of(accountId))
                .amount(ImmutableAmount.of(amountDto.getAmount()))
                .build();
        withdrawalCommandHandler.handle(withdrawal);
    }

    public AccountDto showAccount(Long accountId) {
        AccountId id = ImmutableAccountId.of(accountId);
        return this.accountRepository.findById(id).map(AccountDto::from).orElseThrow(UnexistingAccount::new);
    }
}
