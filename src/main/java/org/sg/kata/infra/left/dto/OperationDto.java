package org.sg.kata.infra.left.dto;

import lombok.Data;
import org.sg.kata.domain.Operation;

import java.time.LocalDateTime;

@Data
public class OperationDto {

    private Long id;
    private LocalDateTime date;
    private String amount;

    public static OperationDto from(Operation operation) {
        String amount = operation.sign().symbol()
                .concat(operation.amount().value().toString())
                .concat(" €");
        OperationDto dto = new OperationDto();
        dto.setId(operation.id().value());
        dto.setDate(operation.date());
        dto.setAmount(amount);
        return dto;
    }
}
