package org.sg.kata.infra.left.dto;

import lombok.Data;

@Data
public class AmountDto {

    private Double amount;
}
