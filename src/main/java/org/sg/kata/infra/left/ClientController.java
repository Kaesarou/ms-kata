package org.sg.kata.infra.left;

import org.sg.kata.infra.configuration.security.AuthenticationService;
import org.sg.kata.infra.left.dto.AuthenticationDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("client")
public class ClientController {

    private final AuthenticationService authenticationService;

    public ClientController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("login")
    public ResponseEntity<String> signIn(@RequestBody AuthenticationDto dto) {
        String token = this.authenticationService.signIn(dto.getUsername(), dto.getPassword());
        return ResponseEntity.ok(token);
    }
}
