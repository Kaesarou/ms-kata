package org.sg.kata.infra.configuration.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.sg.kata.domain.Client;
import org.sg.kata.domain.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class AuthenticationService implements UserDetailsService {

    private final ClientRepository clientRepository;
    private final PasswordEncoder passwordEncoder;
    private final String secret;

    public AuthenticationService(ClientRepository clientRepository,
                                 PasswordEncoder passwordEncoder,
                                 @Value("${jwt.secret}") String secret) {
        this.clientRepository = clientRepository;
        this.passwordEncoder = passwordEncoder;
        this.secret = secret;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Client client = clientRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return new ClientDetails(client.email(), client.password(), client.account().id().value());
    }

    public String signIn(String username, String password) {
        Client client = clientRepository.findByUsername(username)
                .filter(u -> passwordEncoder.matches(password, u.password()))
                .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("bad credentials"));
        Map<String, Object> claims = new HashMap<>();
        //TODO:: constant to avoid this magic string
        claims.put("account_id", client.account().id().value());
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(client.email())
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
}
