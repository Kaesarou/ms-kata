package org.sg.kata.infra.configuration.security;

import lombok.Getter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.util.ArrayList;

public class ClientToken extends UsernamePasswordAuthenticationToken {

    @Getter
    private final Long accountId;

    public ClientToken(Object principal, Long accountId) {
        super(principal, null, new ArrayList<>());
        this.accountId = accountId;
    }
}
