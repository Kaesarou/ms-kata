package org.sg.kata;

import org.sg.kata.domain.Account;
import org.sg.kata.domain.command.Deposit;
import org.sg.kata.domain.command.Withdrawal;
import org.sg.kata.domain.command.handler.CommandHandler;
import org.sg.kata.domain.command.handler.DepositHandler;
import org.sg.kata.domain.command.handler.WithdrawalHandler;
import org.sg.kata.domain.repository.AccountRepository;
import org.sg.kata.domain.repository.OperationRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandHandler<Deposit, Account> depositHandler(AccountRepository accountRepository,
                                                           OperationRepository operationRepository) {
        return new DepositHandler(accountRepository, operationRepository);
    }

    @Bean
    public CommandHandler<Withdrawal, Account> withdrawalHandler(AccountRepository accountRepository,
                                                                 OperationRepository operationRepository) {
        return new WithdrawalHandler(accountRepository, operationRepository);
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
}
