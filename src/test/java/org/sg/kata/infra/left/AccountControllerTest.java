package org.sg.kata.infra.left;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.sg.kata.domain.Account;
import org.sg.kata.domain.ImmutableAccountId;
import org.sg.kata.domain.exception.UnexistingAccount;
import org.sg.kata.domain.repository.AccountRepository;
import org.sg.kata.infra.configuration.security.ClientToken;
import org.sg.kata.infra.left.dto.AccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.sg.kata.domain.Sign.MINUS;
import static org.sg.kata.domain.Sign.PLUS;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    ObjectMapper objectMapper;

    private final ClientToken client = new ClientToken("username", 1L);

    @Test
    @Sql(executionPhase = BEFORE_TEST_METHOD, scripts = "classpath:data.sql")
    public void deposit_should_increase_balance_and_add_operation() throws Exception {
        //GIVEN
        String amount = "{\"amount\":\"200.5\"}";
        //WHEN
        this.mockMvc.perform(post("/account/deposit")
                .contentType(APPLICATION_JSON)
                .content(amount)
                .with(authentication(client)))
                .andExpect(status().isOk());
        //THEN
        Account account = accountRepository.findById(ImmutableAccountId.of(1L))
                .orElseThrow(UnexistingAccount::new);
        assertThat(account.balance().value()).isEqualTo(401);
        assertThat(account.history()).anyMatch(o -> o.sign().equals(PLUS) && o.amount().value().equals(200.5));
    }

    @Test
    @Sql(executionPhase = BEFORE_TEST_METHOD, scripts = "classpath:data.sql")
    public void withdrawal_should_decrease_balance_and_add_operation() throws Exception {
        //GIVEN
        String amount = "{\"amount\":\"200\"}";
        //WHEN
        this.mockMvc.perform(post("/account/withdrawal")
                .contentType(APPLICATION_JSON)
                .content(amount)
                .with(authentication(client)))
                .andExpect(status().isOk());
        //THEN
        Account account = accountRepository.findById(ImmutableAccountId.of(1L))
                .orElseThrow(UnexistingAccount::new);
        assertThat(account.balance().value()).isEqualTo(0.5);
        assertThat(account.history()).anyMatch(o -> o.sign().equals(MINUS) && o.amount().value().equals(200.0));
    }

    @Test
    @Sql(executionPhase = BEFORE_TEST_METHOD, scripts = "classpath:data.sql")
    public void show_history_should_return_ok() throws Exception {
        //WHEN
        String response = this.mockMvc.perform(get("/account/")
                .with(authentication(client)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        //THEN
        AccountDto account = objectMapper.readValue(response, AccountDto.class);
        assertThat(account.getBalance()).isEqualTo(200.5);
        assertThat(account.getHistory()).isEmpty();
    }
}
