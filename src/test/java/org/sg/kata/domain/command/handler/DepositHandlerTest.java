package org.sg.kata.domain.command.handler;

import org.junit.jupiter.api.Test;
import org.sg.kata.ObjectMother;
import org.sg.kata.domain.Account;
import org.sg.kata.domain.ImmutableOperationId;
import org.sg.kata.domain.command.Deposit;
import org.sg.kata.domain.exception.UnexistingAccount;
import org.sg.kata.domain.repository.AccountRepository;
import org.sg.kata.domain.repository.OperationRepository;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DepositHandlerTest {

    private final AccountRepository accountRepository;
    private final OperationRepository operationRepository;
    private final DepositHandler depositHandler;

    public DepositHandlerTest() {
        this.accountRepository = mock(AccountRepository.class);
        this.operationRepository = mock(OperationRepository.class);
        this.depositHandler = new DepositHandler(this.accountRepository, operationRepository);
    }

    @Test
    public void deposit_should_modify_balance_and_history() {
        //GIVEN
        when(accountRepository.findById(any())).thenReturn(Optional.of(ObjectMother.account()));
        when(operationRepository.findNextId()).thenReturn(ImmutableOperationId.of(1L));
        Deposit deposit = ObjectMother.deposit();
        //WHEN
        Account account = this.depositHandler.handle(deposit);
        //THEN
        assertThat(account.balance().value()).isEqualTo(100.25);
        assertThat(account.history()).hasSize(1);
        assertThat(account.history()).anyMatch(o -> o.amount().value().equals(100.25));
    }

    @Test
    public void deposit_should_throw_UnexistingAccount_exception() {
        //GIVEN
        when(accountRepository.findById(any())).thenReturn(Optional.empty());
        Deposit deposit = ObjectMother.deposit();
        //WHEN
        Throwable thrown = catchThrowable(() -> this.depositHandler.handle(deposit));
        //THEN
        assertThat(thrown).isInstanceOf(UnexistingAccount.class);
    }

}
