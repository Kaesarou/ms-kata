package org.sg.kata.domain.command.handler;

import org.junit.jupiter.api.Test;
import org.sg.kata.ObjectMother;
import org.sg.kata.domain.Account;
import org.sg.kata.domain.ImmutableAmount;
import org.sg.kata.domain.ImmutableOperationId;
import org.sg.kata.domain.command.Withdrawal;
import org.sg.kata.domain.exception.NotAllowedOperation;
import org.sg.kata.domain.repository.AccountRepository;
import org.sg.kata.domain.repository.OperationRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WithdrawalHandlerTest {

    private final AccountRepository accountRepository;
    private final OperationRepository operationRepository;
    private final WithdrawalHandler withdrawalHandler;

    public WithdrawalHandlerTest() {
        this.accountRepository = mock(AccountRepository.class);
        this.operationRepository = mock(OperationRepository.class);
        this.withdrawalHandler = new WithdrawalHandler(this.accountRepository, this.operationRepository);
    }

    @Test
    public void withdrawal_should_modify_balance_and_history() {
        //GIVEN
        Account oldAccount = ObjectMother.account().withBalance(ImmutableAmount.of(100.25));
        when(accountRepository.findById(any())).thenReturn(Optional.of(oldAccount));
        when(operationRepository.findNextId()).thenReturn(ImmutableOperationId.of(1L));
        Withdrawal withdrawal = ObjectMother.withdrawal();
        //WHEN
        Account account = this.withdrawalHandler.handle(withdrawal);
        //THEN
        assertThat(account.balance().value()).isEqualTo(50);
        assertThat(account.history()).hasSize(1);
        assertThat(account.history()).anyMatch(o -> o.amount().value().equals(50.25));
    }

    @Test
    public void withdrawal_should_throw_exception() {
        //GIVEN
        when(accountRepository.findById(any())).thenReturn(Optional.of(ObjectMother.account()));
        Withdrawal withdrawal = ObjectMother.withdrawal();
        //WHEN
        Throwable thrown = catchThrowable(() -> this.withdrawalHandler.handle(withdrawal));
        //THEN
        assertThat(thrown).isInstanceOf(NotAllowedOperation.class);
    }
}
