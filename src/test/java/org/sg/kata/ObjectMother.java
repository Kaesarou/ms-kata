package org.sg.kata;

import org.sg.kata.domain.ImmutableAccount;
import org.sg.kata.domain.ImmutableAccountId;
import org.sg.kata.domain.ImmutableAmount;
import org.sg.kata.domain.command.ImmutableDeposit;
import org.sg.kata.domain.command.ImmutableWithdrawal;

import java.util.List;

public class ObjectMother {

    public static ImmutableAccount account() {
        return ImmutableAccount.builder()
                .id(ImmutableAccountId.of(1L))
                .balance(ImmutableAmount.of(0.0))
                .history(List.of()).build();
    }

    public static ImmutableDeposit deposit() {
        return ImmutableDeposit.builder()
                .amount(ImmutableAmount.of(100.25))
                .accountId(ImmutableAccountId.of(1L))
                .build();
    }

    public static ImmutableWithdrawal withdrawal() {
        return ImmutableWithdrawal.builder()
                .amount(ImmutableAmount.of(50.25))
                .accountId(ImmutableAccountId.of(1L))
                .build();
    }
}
