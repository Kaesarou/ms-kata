SET REFERENTIAL_INTEGRITY FALSE;
TRUNCATE TABLE ACCOUNT_HISTORY;
TRUNCATE TABLE CLIENT;
TRUNCATE TABLE OPERATION;
TRUNCATE TABLE ACCOUNT;
SET REFERENTIAL_INTEGRITY TRUE;
INSERT INTO account (id, balance) VALUES (1, 200.5);
INSERT INTO client (id, firstname, lastname, email, password, account_id) VALUES (1, 'firstname', 'lastname', 'account@kata.com', '$2a$10$v5.p6K27UdID3SOYOR1WMe5rECxfYfwHU4OhOSQF2ReEaVNnt6MzK', 1);